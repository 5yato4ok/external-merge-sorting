#include "sorter.h"
#include <time.h>
#include <iostream>
int main() {
  uint32_t max_size = 60 * 1024 * 1024;
  sorter::External_Merge test("input", "output", 2, max_size);
  if (!test.Is_Initialized()) {
    return -1;
  }
  test.Sort_File();
  return 0;
}

