#include "BufferWriter.h"

namespace sorter {
Buffer_Writer::Buffer_Writer(const std::string& file_name, uint32_t max_buffer_size) :
  writing_file(std::ofstream(file_name, std::ios::binary | std::ios::out)),
  max_buf_size(max_buffer_size),
  buf_index (0){}

Buffer_Writer::~Buffer_Writer() {
  flush();
  writing_file.close();
}

void Buffer_Writer::operator << (uint32_t value) {
  if (buf_index >= max_buf_size-1) {
    writeBinaryFile(&cur_writing, 0, cur_writing.size(), writing_file);
    cur_writing.clear();
    buf_index = 1;
  }
  buf_index += 1;
  cur_writing.push_back(value);
}

void Buffer_Writer::flush() {
  if (cur_writing.size()) {
    writeBinaryFile(&cur_writing, 0, cur_writing.size(), writing_file);
    cur_writing.clear();
  }
}

void Buffer_Writer::writeBinaryFile(std::vector<uint32_t> * buffer, uint32_t bpos, uint32_t size, std::ofstream & io) {
  io.write(reinterpret_cast<const char*>(&(*buffer)[bpos]), size * sizeof(uint32_t));
  io.flush();
}

}//namespace sorter
