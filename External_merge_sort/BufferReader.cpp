#include "BufferReader.h"

namespace sorter {

Buffer_Reader::Buffer_Reader(const std::string& file_name, uint32_t max_buffer_size, uint32_t block_num_):
  reading_file(std::ifstream(file_name,std::ios::binary| std::ios::in)),
  block_num(block_num_),
  max_buf_size(max_buffer_size),
  is_readable_(false){
  reading_file.seekg(0, std::ios::end);
  file_size =(uint32_t) reading_file.tellg();
  if (file_size > 0 && reading_file.is_open()) {
    is_readable_ = true;
  }
  reading_file.seekg(0);
  pos_in_file = 0;
  buf_index = 0;
}

void Buffer_Reader::Close() {
  reading_file.close();
}

Merger Buffer_Reader::Get_next() {
  if ((buf_index > (cur_reading.size()-1) || !buf_index) && is_readable_ 
    && (pos_in_file < file_size)) {
    read_next_part();
    buf_index = 0;
  }
  Merger result_value = { cur_reading[buf_index],block_num };
  buf_index += 1;
  if ((pos_in_file >= file_size) && (buf_index > (cur_reading.size() - 1))) {
    is_readable_ = false;
  }
  return result_value;
}

void Buffer_Reader::read_next_part() {
  cur_reading.clear();
  uint32_t read_size = (pos_in_file + max_buf_size) > file_size ?  file_size - pos_in_file: max_buf_size;
  if (read_size != max_buf_size) {
    int test = 2;
  }
  cur_reading = std::vector<uint32_t>(read_size / sizeof(uint32_t));
  readBinaryFile(&cur_reading, 0, pos_in_file, read_size, reading_file);
  pos_in_file += read_size;

}

void Buffer_Reader::readBinaryFile(std::vector<uint32_t> * buffer, uint32_t bpos, uint32_t pos, uint32_t bytes, std::ifstream & io) {
  io.seekg(pos, std::ios_base::beg);
  io.read(reinterpret_cast<char*>(&(*buffer)[bpos]), bytes);
}

Buffer_Reader::~Buffer_Reader() {
  if (reading_file.is_open()) {
    reading_file.close();
  }
}

}//namespace sorter
