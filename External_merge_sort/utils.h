#ifndef UTILS_H
#define UTILS_H
#include <stdint.h>

namespace sorter {

//int compare(const void * a, const void * b);

struct Merger {
  uint32_t data;
  uint32_t block_numb;
};

struct GreaterThan {
  bool operator()(const Merger& lhs, const Merger& rhs) const {
    return lhs.data > rhs.data;
  }
};

} // namespace sorter
#endif
