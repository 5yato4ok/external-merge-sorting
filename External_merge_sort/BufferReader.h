#ifndef BUF_READ_H
#define BUF_READ_H
#include <string>
#include <fstream>
#include <vector>
#include "utils.h"
#include <stdint.h>

namespace sorter {
class Buffer_Reader {
 public:
  Buffer_Reader(const std::string& file_name, uint32_t max_buffer_size, uint32_t block_num);
  Buffer_Reader(const Buffer_Reader&) = delete;
  Buffer_Reader& operator=(const Buffer_Reader& copy) = delete;
  ~Buffer_Reader();
  void Close();
  bool Is_readable() { return is_readable_; };
  Merger Get_next();
 private:
  std::ifstream reading_file;
  std::vector<uint32_t> cur_reading;
  void read_next_part();
  void readBinaryFile(std::vector<uint32_t> * buffer, uint32_t bpos, uint32_t pos, uint32_t bytes,std::ifstream & io);
  uint32_t pos_in_file;
  uint32_t buf_index;
  const uint32_t max_buf_size;
  uint32_t block_num;
  uint32_t file_size;
  bool is_readable_;
};
}//namespace sorter

#endif
