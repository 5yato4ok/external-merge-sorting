#include "sorter.h"

namespace sorter {

External_Merge::External_Merge(const std::string& input_file_name_, const std::string& output_file_name_,
  uint8_t thread_count_, uint32_t max_buffer_size_) :
  input_file_name(move(input_file_name_)),
  output_file_name(move(output_file_name_)),
  thread_count(thread_count_),
  max_block_size(max_buffer_size_),
  run_counter(0),
  is_initialized_(true){
  std::ifstream input(input_file_name, std::ios::binary| std::ios::ate);
  file_size = (uint32_t)input.tellg();
  if (!input.is_open() || file_size <= 0 || thread_count <= 0 || max_block_size <= 0) {
    is_initialized_ = false;
    input.close();
    return;
  }
  input.close();
  if (max_block_size > file_size) {
    max_block_size = file_size;
  } else if (max_block_size%number_size) {
    correct_max_block_size();
  }
  block_per_thread = max_block_size / thread_count;
}

void External_Merge::run_sorting_thread(uint32_t position, uint32_t block_size) {
  std::ifstream in(input_file_name, std::ios::binary | std::ios::in);
  std::vector<uint32_t> buff(block_size / number_size);
  in.seekg(position);
  in.read(reinterpret_cast<char*>(buff.data()), block_size);
  in.close();
  sort(buff.begin(), buff.end());
  write_to_tmp(buff);
}

void External_Merge::write_to_tmp(const std::vector<uint32_t>& buffer) {
  mut.lock();
  auto file_name = std::to_string(buffer.size()*number_size)+"_"+std::to_string(run_counter);
  remaining_files.push_back(file_name);
  run_counter += 1;
  mut.unlock();
  std::ofstream out_tmp_fs(file_name, std::ios::binary);
  out_tmp_fs.write((char*)buffer.data(), buffer.size()*number_size);
  out_tmp_fs.close();
}

void External_Merge::correct_max_block_size() {
  int q = max_block_size / number_size;
  int n1 = number_size * q;
  int n2 = number_size * (q - 1);
  if (abs((int32_t)max_block_size - n1) < abs((int32_t)max_block_size - n2)) {
    max_block_size = n1;
  } else {
    max_block_size = n2;
  }
}

void External_Merge::merging2files(std::string& l_name, std::string& r_name, 
  const std::string& result_name,uint32_t cur_merging_size) {
  uint32_t buf_size = cur_merging_size / 4;
  std::ofstream result_tmp(result_name, std::ios::binary);
  Buffer_Reader l_file(l_name, buf_size, 0);
  Buffer_Reader r_file(r_name, buf_size, 1);
  Buffer_Writer merge_result(result_name, buf_size);
  std::priority_queue<Merger, std::vector<Merger>, GreaterThan> compare;
  compare.push(l_file.Get_next());
  compare.push(r_file.Get_next());
  while (!compare.empty()) {
    auto lowest = compare.top();
    compare.pop();
    merge_result << lowest.data;
    if (lowest.block_numb && r_file.Is_readable()) {
      compare.push(r_file.Get_next());
    } else if (!lowest.block_numb&&l_file.Is_readable()) {
      compare.push(l_file.Get_next());
    }
  }
  merge_result.flush();
  l_file.Close();
  r_file.Close();
  std::remove(l_name.c_str());
  std::remove(r_name.c_str());
}

void External_Merge::merge_tmp() {
  uint32_t cur_merging_size = block_per_thread;
  uint32_t cur_blocks_nums = file_size / block_per_thread;
  if (cur_blocks_nums % 2 != 0) {
    cur_blocks_nums -= 1;
  }
  uint32_t size_of_addition = file_size - block_per_thread*cur_blocks_nums;
  while (cur_merging_size + size_of_addition < file_size) {
    for (uint32_t l_block = 0; l_block < cur_blocks_nums; l_block+=2) {
      std::string l_name = remaining_files[0];
      std::string r_name = remaining_files[1];
      std::string result_name = std::to_string(cur_merging_size * 2) + "_" + std::to_string(l_block / 2);
      merging2files(l_name, r_name,result_name,max_block_size);
      remaining_files.erase(remaining_files.begin(), remaining_files.begin() + 2);
      remaining_files.push_back(result_name);
    }
    cur_blocks_nums = cur_blocks_nums / 2;
    cur_merging_size += cur_merging_size;
  }
  while (!remaining_files.empty()) {
    if (remaining_files.size()==1) {
      rename(remaining_files[0].c_str(), "output");
      remove(remaining_files[0].c_str());
      remaining_files.clear();
      break;
    }
    auto l_file_name = remaining_files[0];
    std::ifstream l_file (l_file_name,std::ios::ate);
    uint32_t l_size = (uint32_t)l_file.tellg();
    l_file.close();

    auto r_file_name = remaining_files[1];
    std::ifstream r_file(r_file_name, std::ios::ate);
    uint32_t r_size = (uint32_t)r_file.tellg();
    r_file.close();

    std::string result_name = std::to_string(l_size+r_size);
    merging2files(l_file_name, r_file_name,result_name,max_block_size);
    remaining_files.erase(remaining_files.begin(), remaining_files.begin() + 2);
    remaining_files.push_back(result_name);
  }
}

void External_Merge::Sort_File() {
  for (uint32_t pos = 0; pos < file_size;) {
    std::vector<std::thread> threads;
    for (uint32_t i = 0; i < thread_count; i++, pos += block_per_thread) {
      if (pos >= file_size) {
        break;
      }
      uint32_t block_size = block_per_thread;
      if (pos + block_per_thread > file_size) {
        block_size = file_size - pos;
      }
      threads.push_back(std::thread(&External_Merge::run_sorting_thread, this, pos, block_size));
    }
    for (auto& th : threads) {
      th.join();
    }
  }
  merge_tmp();
}

} //namespace sorter
