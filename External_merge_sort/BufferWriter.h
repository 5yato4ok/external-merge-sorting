#ifndef BUF_WRITE_H
#define BUF_WRITE_H
#include <string>
#include <fstream>
#include <vector>
#include <stdint.h>

namespace sorter {
class Buffer_Writer {
public:
  Buffer_Writer(const std::string& file_name, uint32_t max_buffer_size);
  Buffer_Writer(const Buffer_Writer&) = delete;
  Buffer_Writer& operator=(const Buffer_Writer& copy) = delete;
  ~Buffer_Writer();
  void operator<<(uint32_t value);
  void flush();
 private:
  std::ofstream writing_file;
  std::vector<uint32_t> cur_writing;
  void writeBinaryFile(std::vector<uint32_t> * buffer, uint32_t bpos, uint32_t size, std::ofstream & io);
  uint32_t buf_index;
  const uint32_t max_buf_size;

};
}//namespace sorter

#endif
