#ifndef SORTER_H
#define SORTER_H
#include "utils.h"
#include "BufferReader.h"
#include "BufferWriter.h"
#include <string>
#include <stdint.h>
#include <thread>
#include <mutex>
#include <fstream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <queue>

namespace sorter {

class External_Merge {
 public:
  External_Merge(const std::string& input_file_name, const std::string& output_file_name, 
    uint8_t thread_count, uint32_t max_buffer_size);
  External_Merge(const External_Merge& copy) = delete;
  External_Merge& operator=(const External_Merge& copy) = delete;
  bool Is_Initialized() { return is_initialized_; }
  void Sort_File();
 private:
  std::vector<std::string> remaining_files;
  void correct_max_block_size();
  void run_sorting_thread(uint32_t position, uint32_t block_size);
  void write_to_tmp(const std::vector<uint32_t>& buffer);
  void merge_tmp();
  void merging2files(std::string& l_name, std::string& r_name,
    const std::string& result_name, uint32_t cur_merging_size);
  const int number_size = sizeof(uint32_t);
  const std::string input_file_name;
  const std::string output_file_name;
  const uint8_t thread_count;
  uint32_t max_block_size;
  uint32_t block_per_thread;
  uint32_t file_size;
  std::mutex mut;
  uint32_t run_counter;
  bool is_initialized_;
};

} //namespace sorter

#endif
