# External Multithreading Merge Sorting

*External sorting* is a term for a class of sorting algorithms that can handle massive amounts of data. External sorting is required when the data being sorted do not fit into the main memory of a computing device (usually RAM) and instead they must reside in the slower external memory (usually a hard drive). 

External sorting typically uses a hybrid sort-merge strategy. In the sorting phase, chunks of data small enough to fit in main memory are read, sorted, and written out to a temporary file. In the merge phase, the sorted sub-files are combined into a single larger file.

This project is an example of external merge sort algorithm, which sorts chunks that each fit in RAM, then merges the sorted chunks together.

# Description of classes

* sorter::External_Merge - the main sort class.
* sorter::Buffer_Reader - helper class for reading big file by chunks.
* sorter::Buffer_Writer - helper class for writing block of data in output file.
